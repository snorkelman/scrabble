ifeq ($(origin CXX), default)
CXX = g++
endif

GTEST_INCLUDE = googletest/googletest/include
GTEST_LIB = googletest/build/googlemock/gtest

BENCHMARK_INCLUDE = benchmark/include
BENCHMARK_LIB = benchmark/build/src

all: app cli tests benchmarks

clean:
	rm -f app app.o cli cli.o server.o tests tests.o benchmarks benchmarks.o battle battle.o board.o

app: src/app/app.cpp include/lib/iserver.h server.o
	$(CXX) -o app -I . -I include/lib -I CLI11/include src/app/app.cpp server.o -lpthread

battle: server.o battle.o board.o 
	$(CXX) -o battle board.o battle.o server.o -L 0 -lPlayer0 -L 1 -lPlayer1 -lpthread

battle.o: src/battle/bag.h src/battle/battle.cpp src/battle/board.h src/battle/dictionary.h
	$(CXX) -I 0 -I 1 -I include/lib -I include/battle -c src/battle/battle.cpp 

board.o: src/battle/bag.h src/battle/board.cpp src/battle/board.h src/battle/dictionary.h
	$(CXX) -I 0 -I 1 -I include/lib -c src/battle/board.cpp 

cli: src/cli/cli.cpp include/lib/iserver.h server.o
	$(CXX) -o cli -I . -I include/lib -I CLI11/include src/cli/cli.cpp server.o -lpthread

server.o: src/lib/server.cpp include/lib/server.h include/lib/iserver.h
	$(CXX) -Iinclude/lib -I. -c src/lib/server.cpp

tests: src/test/tests.cpp googletest/build/googlemock/gtest/libgtest.a
	$(CXX) -I$(GTEST_INCLUDE) -I include/lib -o tests src/test/tests.cpp -L$(GTEST_LIB) -lgtest -lgtest_main -lpthread

benchmarks: src/test/benchmarks.cpp benchmark/build/src/libbenchmark.a
	$(CXX) -o benchmarks -I$(BENCHMARK_INCLUDE) -I include/lib src/test/benchmarks.cpp -L$(BENCHMARK_LIB) -lbenchmark -lpthread

benchmark/build/src/libbenchmark.a:
	mkdir benchmark/build ; cd benchmark/build ; cmake -DCMAKE_BUILD_TYPE=Release -DGOOGLETEST_PATH=../../googletest ../ ; make

googletest/build/googlemock/gtest/libgtest.a:
	mkdir googletest/build ; cd googletest/build ; cmake .. ; make

