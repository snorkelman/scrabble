#include "gtest/gtest.h"

#include <iostream>
#include <sstream>

#include "combo.h"


TEST(Combinations, Passes)
{
  auto results = combinations("ABC");
  // AB AC BA BC CA CB ABC ACB BAC BCA CAB CBA
  ASSERT_EQ(results.size(), 12);
  ASSERT_NE(results.find("AB"), results.end()); //1
  ASSERT_NE(results.find("AC"), results.end()); //2
  ASSERT_NE(results.find("BA"), results.end()); //3
  ASSERT_NE(results.find("BC"), results.end()); //4
  ASSERT_NE(results.find("CA"), results.end()); //5
  ASSERT_NE(results.find("CB"), results.end()); //6
  ASSERT_NE(results.find("ABC"), results.end()); //7
  ASSERT_NE(results.find("ACB"), results.end()); //8
  ASSERT_NE(results.find("BAC"), results.end()); //9
  ASSERT_NE(results.find("BCA"), results.end()); //10
  ASSERT_NE(results.find("CAB"), results.end()); //11
  ASSERT_NE(results.find("CBA"), results.end()); //12
}

TEST(Combinations, PassesWithDuplicates)
{
  auto results = combinations("AAAAA");
  // AA AAA AAAA AAAAA
  ASSERT_EQ(results.size(), 4);
  ASSERT_NE(results.find("AA"), results.end()); //1
  ASSERT_NE(results.find("AAA"), results.end()); //2
  ASSERT_NE(results.find("AAAA"), results.end()); //3
  ASSERT_NE(results.find("AAAAA"), results.end()); //4
}
