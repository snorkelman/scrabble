#include "server.h"

#include <cctype>
#include <chrono>
#include <cstring>
#include <functional>
#include <iostream>
#include <sstream>
#include <thread>
#include <vector>

#ifdef __WIN32
#pragma warning( push [ , n ] )
#endif

#pragma warning(disable: 4267 4800 4996)
#define CPPHTTPLIB_THREAD_POOL_COUNT 12
#include "cpp-httplib/httplib.h"

#ifdef __WIN32
#pragma warning( pop )
#endif

constexpr std::chrono::seconds Server::m_longPollTimeout;

Server::Server()
: m_player_1_rack("")
, m_player_2_rack("")
, m_board(255, ' ')
, m_player1(0)
, m_player2(0)
, m_player_1_name("Human")
, m_player_2_name("Computer")
, m_thread(std::bind(&Server::http_server, this))
{
  std::this_thread::sleep_for(std::chrono::milliseconds(1500));
  forceServe();
}

Server::~Server()
{
  if (!m_shutDown) {
    stop();
  }
}
void Server::forceServe()
{
  {
    std::lock_guard<std::mutex> lock(m_scoreMutex);
    m_newScoreData = true;
  }
  m_scoreConditionVariable.notify_one();

  {
    std::lock_guard<std::mutex> lock(m_boardMutex);
    m_newBoardData = true;
  }
  m_boardConditionVariable.notify_one();

  {
    std::lock_guard<std::mutex> lock(m_rackMutex);
    m_newRackData = true;
  }
  m_rackConditionVariable.notify_one();

  {
    std::lock_guard<std::mutex> lock(m_historyMutex);
    m_newHistoryData = true;
  }
  m_historyConditionVariable.notify_one();
}

void Server::setRack(const std::string &player_1_letters, const std::string &player_2_letters)
{
  std::lock_guard<std::mutex> lock(m_rackMutex);
  m_player_1_rack = player_1_letters;
  m_player_2_rack = player_2_letters;
  m_newRackData = true;
  m_rackConditionVariable.notify_one();
}

void Server::addHistory(const std::string &player_name, const std::string &move, int score )
{
  std::lock_guard<std::mutex> lock(m_historyMutex);
  std::stringstream ss;
  ss << player_name << "," << move << "," << score;
  if (m_history.size()) {
    m_history += "\n";
  }
  m_history += ss.str();
  m_newHistoryData = true;
  m_historyConditionVariable.notify_one();
}


void Server::lay(bool across, const std::string &position, const std::string &letters)
{
  std::stringstream ss(position);
  char row;
  int column;
  ss >> row >> column;
  int offset = (std::tolower(row)-'a') * 15 + column - 1;
  if (offset < 0 || offset > 224) {
    std::cout << "lay: error in starting position: " << position << "\n";
  } else {
    std::lock_guard<std::mutex> lock(m_boardMutex);
    for (int i = 0 ; i < letters.size() ; i++) 
    {
      if (column > 15 || offset >= 225) {
        std::cout << "lay: overflow laying tiles\n";
        break;
      }
      m_board[offset] = letters[i];
      if (across) {
        offset++;
        column++;
      } else {
        offset += 15;
      }
    }
    m_newBoardData = true;
    m_boardConditionVariable.notify_one();
  }
}

void Server::setScores(int player1, int player2)
{
  std::lock_guard<std::mutex> lock(m_scoreMutex);
  m_player1 = player1;
  m_player2 = player2;
  m_newScoreData = true;
  m_scoreConditionVariable.notify_one();
}

void Server::setNames(const std::string &player_1_name, const std::string &player_2_name)
{
  std::lock_guard<std::mutex> lock(m_scoreMutex);
  m_player_1_name = player_1_name;
  m_player_2_name = player_2_name;
  m_newScoreData = true;
  m_scoreConditionVariable.notify_one();
}

void Server::reset()
{
  setScores(0, 0);
  setNames("Human", "Computer");
  setRack("", "");
  std::lock_guard<std::mutex> lock(m_boardMutex);
  m_board = std::string(225, ' ');
  m_newBoardData = true;
  m_boardConditionVariable.notify_one();
}

void Server::stop()
{
  m_shutDown = true;
  // std::cout << "Stopping\n";
  httplib::Client cli(m_host, m_port);
  auto res = cli.Get("/stop");
  forceServe();
  m_thread.join();
}

void Server::http_server()
{
    httplib::Server svr;

    svr.Get("/hi", [](const httplib::Request& req, httplib::Response& res) {
      //std::cout << "-->/hi\n";
      res.set_content("Hello World!", "text/plain");
      //std::cout << "<--/hi\n";
    });

    svr.Get("/init", [&](const httplib::Request& req, httplib::Response& res) {
      //std::cout << "-->/init\n";
      forceServe();
      res.set_content("Restarting long polls", "text/plain");
      //std::cout << "<--/init\n";
    });

    svr.Get("/board", [&](const httplib::Request& req, httplib::Response& res) {
      //std::cout << "-->/board\n";
      std::unique_lock<std::mutex> lock(m_boardMutex);
      if (!m_shutDown) {
        m_boardConditionVariable.wait_for(lock, m_longPollTimeout, [&]{ return m_newBoardData == true; } );
        m_newBoardData = false;
      }
      res.set_content(m_board, "text/plain");
      //std::cout << "<--/board\n";
    });

    svr.Get("/rack", [&](const httplib::Request& req, httplib::Response& res) {
      //std::cout << "-->/rack\n";
      std::unique_lock<std::mutex> lock(m_rackMutex);
      if (!m_shutDown) {
        m_rackConditionVariable.wait_for(lock, m_longPollTimeout, [&]{ return m_newRackData == true; } );
        m_newRackData = false;
      }
      res.set_content(m_player_1_rack + "," + m_player_2_rack, "text/plain");
      //std::cout << "<--/rack\n";
    });

    svr.Get("/score", [&](const httplib::Request& req, httplib::Response& res) {
      //std::cout << "-->/score\n";
      std::unique_lock<std::mutex> lock(m_scoreMutex);
      if (!m_shutDown) {
        m_scoreConditionVariable.wait_for(lock, m_longPollTimeout, [&]{ return m_newScoreData == true; } );
        m_newScoreData = false;
      }
      std::stringstream s;
      s << m_player1 << "," << m_player2 << "," << m_player_1_name << "," << m_player_2_name;
      res.set_content(s.str(), "text/plain");
      //std::cout << "<--/score\n";
    });

    svr.Get("/history", [&](const httplib::Request& req, httplib::Response& res) {
      //std::cout << "-->/history\n";
      std::unique_lock<std::mutex> lock(m_historyMutex);
      if (!m_shutDown) {
        m_historyConditionVariable.wait_for(lock, m_longPollTimeout, [&]{ return m_newHistoryData == true; } );
        m_newHistoryData = false;
      }
      res.set_content(m_history, "text/plain");
      //std::cout << "<--/history\n";
    });

    svr.Get("/stop", [&](const httplib::Request& req, httplib::Response& res) {
      //std::cout << "-->/stop\n";
      svr.stop();
      //std::cout << "<--/stop\n";
    });
   
    svr.set_base_dir("./html", "/");
    svr.set_base_dir("./assets", "/");
    svr.set_base_dir("./script", "/");
    svr.set_file_extension_and_mimetype_mapping("html", "text/html");
    svr.set_file_extension_and_mimetype_mapping("png", "image/png");
    svr.listen(m_host, m_port);
    //std::cout << "Exiting server thread\n";
}
