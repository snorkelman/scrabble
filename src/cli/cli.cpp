#include <cctype>
#include <functional>
#include <iostream>
#include <map>
#include <sstream>
#include <string>

#include <CLI/CLI.hpp>

#include "iserver.h"
#include "server.h"

class CliData {
public:
  CliData(IServer &server);
  void parse(std::string &commandLine) {
    m_app.parse(commandLine);
  }
  bool done() { return m_done; }
 private:
  IServer &m_server;
  CLI::App m_app{"CLI interface to the Scrabble server"};

  void helpCallback();
  void rackCallback();
  void acrossCallback();
  void downCallback();
  void scoreCallback();
  void namesCallback();
  void resetCallback();
  void stopCallback();
  void historyCallback();

  std::string helpTopicValidator(std::string& topic);
  std::string rackLettersValidator(std::string &letters);
  std::string boardLettersValidator(std::string &letters);
  std::string positionValidator(std::string &position);
  std::map<std::string, CLI::App*> m_subCommands;
  std::string m_stringArg1;
  std::string m_stringArg2;
  int m_intArg1;
  int m_intArg2;
  std::string m_appHelpString;
  bool m_done;
};

CliData::CliData(IServer &server)
: m_server(server)
, m_done(false)
{
  using namespace std::placeholders;
  m_app.set_help_flag("", "");
  m_app.require_subcommand(-1);
  m_subCommands["rack"] = m_app.add_subcommand("rack", "Set the characters in the rack. A maximum\n"
                                                       "of seven characters, using the ! for a blank.")
                       ->final_callback(std::bind(&CliData::rackCallback, this));
  m_subCommands["rack"]->add_option("p1_letters", m_stringArg1, "The letters to be inserted in player 1's rack,\n"
                                                                "using ! to insert a blank." )
	               ->required()
                       ->check(CLI::Validator(std::bind(&CliData::rackLettersValidator, this, _1), "", ""));
  m_subCommands["rack"]->add_option("p2_letters", m_stringArg2, "The letters to be inserted in player 2's rack,\n"
                                                                "using ! to insert a blank. If this optional parameter\n"
		                                                "is non-blank, the second rack will appear on the board" )
                       ->check(CLI::Validator(std::bind(&CliData::rackLettersValidator, this, _1), "", ""));
  m_subCommands["across"] = m_app.add_subcommand("across", "Write characters across the board, starting\n"
                                                           "at the given board position. The board position\n"
                                                           "is given as a two or three character token composed\n"
                                                           "of the row letter and column number. Insert blanks\n" 
                                                           "using the ! character.")
                         ->final_callback(std::bind(&CliData::acrossCallback, this));
  m_subCommands["across"]->add_option("position", m_stringArg1, "The position to start placing the letters, given\n"
                                                                "as a two or three character token composed of the\n"
                                                                "row leter and column number")
                         ->required()
                         ->check(CLI::Validator(std::bind(&CliData::positionValidator, this, _1), "", ""));
  m_subCommands["across"]->add_option("letters", m_stringArg2, "The letters to be inserted on the board,\n"
                                                               "using ! to insert a blank. Capital letters\n" 
                                                               "are used to denote a converted blank")
                         ->required()
                         ->check(CLI::Validator(std::bind(&CliData::boardLettersValidator, this, _1), "", ""));
  m_subCommands["down"] = m_app.add_subcommand("down", "Write characters down the board, starting\n"
                                                       "at the given board position. The board position\n"
                                                       "is given as a two or three character token composed\n"
                                                       "of the row letter and column number. Insert blanks\n" 
                                                       "using the ! character\n")
                       ->final_callback(std::bind(&CliData::downCallback, this));
  m_subCommands["down"]->add_option("position", m_stringArg1, "The position to start placing the letters, given\n"
                                                              "as a two or three character token composed of the\n"
                                                              "row leter and column number")
                       ->required()
                       ->check(CLI::Validator(std::bind(&CliData::positionValidator, this, _1), "", ""));
  m_subCommands["down"]->add_option("letters", m_stringArg2, "The letters to be inserted on the board,\n"
                                                             "using ! to insert a blank. Capital letters\n" 
                                                             "are used to denote a converted blank")
                       ->required()
                       ->check(CLI::Validator(std::bind(&CliData::boardLettersValidator, this, _1), "", ""));
  m_subCommands["score"] = m_app.add_subcommand("score", "Set the scores\n")
                        ->final_callback(std::bind(&CliData::scoreCallback, this));
  m_subCommands["score"]->add_option("Player1", m_intArg1, "Player 1 score")
                        ->required()
                        ->check(CLI::detail::Number());
  m_subCommands["score"]->add_option("Player2", m_intArg2, "Player 2 score")
                        ->required()
                        ->check(CLI::detail::Number());
  m_subCommands["names"] = m_app.add_subcommand("names", "Set the player names\n")
                        ->final_callback(std::bind(&CliData::namesCallback, this));
  m_subCommands["names"]->add_option("Player1 Name", m_stringArg1, "Player 1 name")
                        ->required();
  m_subCommands["names"]->add_option("Player2 Name", m_stringArg2, "Player 2 name")
                        ->required();
  m_subCommands["reset"] = m_app.add_subcommand("reset", "Reset the rack, board, and scores\n")
                        ->final_callback(std::bind(&CliData::resetCallback, this));
  m_subCommands["stop"] = m_app.add_subcommand("stop", "Stop the server and exit.\n")
                       ->final_callback(std::bind(&CliData::stopCallback, this));
  m_subCommands["help"] = m_app.add_subcommand("help", "Print help.\n")
                          ->final_callback(std::bind(&CliData::helpCallback, this));
  m_subCommands["help"]->add_option("helpTopic", m_stringArg1, "")
                       ->check(CLI::Validator(std::bind(&CliData::helpTopicValidator, this, _1), "", ""));
  m_subCommands["history"] = m_app.add_subcommand("play", "Add a move to the history. Play data is composed of\n"
                                                       "a player name, move, and score.")
                         ->final_callback(std::bind(&CliData::historyCallback, this));
  m_subCommands["history"]->add_option("Player Name", m_stringArg1, "Player name")
                        ->required();
  m_subCommands["history"]->add_option("Move", m_stringArg2, "Move")
                        ->required();
  m_subCommands["history"]->add_option("Score", m_intArg1, "Score")
                        ->required();
  for ( auto p : m_subCommands) {
    p.second->set_help_flag("","");
  }
  m_appHelpString = m_app.help();
}

std::string CliData::helpTopicValidator(std::string& topic)
{
  auto p = m_subCommands.find(topic);
  if (p != m_subCommands.end())
    return "";
  else
    return "Unknown help topic";
}

void CliData::helpCallback()
{
  if (m_stringArg1.size() == 0)
  {
    int lineno = 0;
    std::string line;
    std::stringstream ss(m_appHelpString);
    std::cout << "Commands:\n";
    while ( std::getline(ss, line) ) {
      if (lineno++ > 3 &&  line.size() ) {
        std::cout << line << "\n";
      } 
    }
  }
  else
  {
    std::cout << m_subCommands[m_stringArg1]->help() << "\n";
  }
}

std::string CliData::rackLettersValidator(std::string &letters)
{
  if (letters.size() > 7 ) 
  {
    return "Too many letters";
  }
  for (auto c : letters) {
    c = std::tolower(c);
    if (c != '!' && (c < 'a' || c > 'z') )
    {
      return "Illegal letters";
    }
  }
  return "";
}

std::string CliData::boardLettersValidator(std::string &letters)
{
  if (letters.size() > 15 ) 
  {
    return "Too many letters";
  }
  for (auto c : letters) {
    c = std::tolower(c);
    if (c != '!' && (c < 'a' || c > 'z') )
    {
      return "Illegal letters";
    }
  }
  return "";
}

void CliData::rackCallback()
{
  for (auto &c : m_stringArg1 ) {
    c = std::tolower(c);
  }
  for (auto &c : m_stringArg2 ) {
    c = std::tolower(c);
  }
  if (m_stringArg2 == "" ) {
    std::cout << "Setting rack to " << m_stringArg1 << "\n";
  } else {
    std::cout << "Setting racks to " << m_stringArg1 << ", " << m_stringArg2 << "\n";
  }
  m_server.setRack(m_stringArg1, m_stringArg2);
}

std::string CliData::positionValidator(std::string &position)
{
  if (position.size() < 2 || position.size() > 3) {
    return "Invalid format for position";
  }
  if (std::tolower(position[0]) < 'a' || std::tolower(position[0]) > 'o') {
    return "Invalid row in position";
  }
  if (position[1] < '0' || position[1] > '9') {
    return "Non-numeric column data in position";
  }
  if (position.size() == 3 ) {
    if (position[2] < '0' || position[2] > '9') {
      return "Non-numeric column data in position";
    }
  }
  int i = std::stoi(position.substr(1));
  if ( i < 1 || i > 15) {
      return "Invalid column number in position";
  }
  return "";
}

void CliData::acrossCallback()
{
  for (auto &c : m_stringArg1 ) {
    c = std::tolower(c);
  }
  m_server.layAcross(m_stringArg1, m_stringArg2);
}

void CliData::downCallback()
{
  for (auto &c : m_stringArg1 ) {
    c = std::tolower(c);
  }
  m_server.layDown(m_stringArg1, m_stringArg2);
}

void CliData::scoreCallback()
{
  m_server.setScores(m_intArg1, m_intArg2);
}

void CliData::namesCallback()
{
  m_server.setNames(m_stringArg1, m_stringArg2);
}

void CliData::resetCallback()
{
  m_server.reset();
}

void CliData::stopCallback()
{
  m_done = true;
}

void CliData::historyCallback()
{
  m_server.addHistory(m_stringArg1, m_stringArg2, m_intArg1);
}

int main() 
{
  std::string line;
  Server server;
  for ( ; ; ) {
    std::cout << "> ";
    std::getline(std::cin, line);
    CliData cliData(server);
    try {
      cliData.parse(line);
    }
    catch (const CLI::ParseError &e)
    {
      std::cout << e.what() << "\n";
    }
    if (cliData.done())
    {
      break;
    }
  }
  return 0;
}

