#ifndef BAG_DOT_H
#define BAG_DOT_H

#include <algorithm>
#include <cctype>
#include <ctime>
#include <exception>
#include <fstream>
#include <map>
#include <string>
#include <vector>

namespace Battle {

  class Bag {
   public:
    Bag()
    {
      std::ifstream file("letter_counts_scores.txt");
      if (!file) {
        throw std::logic_error("Could not open letter_counts_scores.txt");
      }
      char c;
      int count;
      int value;
      while (file >> c >> count >> value) {
        for (int i = 0 ; i < count ; i++ ) {
          m_tiles.push_back(std::tolower(c));
        }
        m_scores[std::tolower(c)] = value;
      }
      randomize();
    }
    bool empty() {
      return m_tiles.size() == 0;
    }
    std::string draw(std::string rack) {
      while (rack.size() < 7 && !empty()) {
        char c = m_tiles.back();
        m_tiles.pop_back();
        rack += c;
      }
      return rack;
    }
    std::string discard(std::string rack, std::string discards)
    {    
      for (char c : discards) {
        std::string::size_type pos;
        if (std::isupper(c)) {
          pos = rack.find('!');
        } else {
          pos = rack.find(c);
        }
        if (pos == std::string::npos) {        
          throw std::logic_error("Error discarding character" );
        }
        rack.erase(pos,1);
      }
      return draw(rack);
    }

    std::string swap(std::string rack, std::string swaps)
    {    
      for (char c : swaps) {
        auto pos = rack.find(c);
        if (pos == std::string::npos) {        
          throw std::logic_error("Error swapping character" );
        }
        rack.erase(pos,1);
        giveBack(c);
      }
      randomize();
      return draw(rack);
    }

    bool checkPlay(std::string rack, std::string play) {
      for (char c : play) {
        std::string::size_type pos;
        if (std::isupper(c)) {
          pos = rack.find('!');
        } else {
          pos = rack.find(c);
        }
        if (pos == std::string::npos) {
          std::cout << "Error on char " << c << " rack is now: " << rack << "\n";
          return false;
        }
        rack.erase(pos,1);
      }
      return true;
    }
    std::map<char,int> m_scores;
   private :
    void randomize()
    {
      std::srand(static_cast<unsigned int>(std::time(nullptr)));
      for (int i = static_cast<int>(m_tiles.size()) - 1 ; i > 0 ; i-- ) {
        int j = std::rand() % (i+1);
        std::swap(m_tiles[i], m_tiles[j]);
      }
    }
    void giveBack(char c) {
      m_tiles.push_back(c);
    }
    std::vector<char> m_tiles;
  };

}

#endif // #ifndef BAG_DOT_H
