#include <chrono>
#include <iostream>
#include <sstream>
#include <thread>

#include "bag.h"
#include "board.h"
#include "dictionary.h"
#include "server.h"
#include "playerif.h"
#include "player0.h"
#include "player1.h"


std::ostream &operator<<(std::ostream &s, const Battle::Play &rhs)
{
  s << char(rhs.m_row+'A') << (rhs.m_col+1) << "-" << (rhs.m_across ? "A" : "D") << " " << rhs.m_tiles;
  return s;
}

void print_prefix(Battle::PlayerIf *pPlayer, std::chrono::duration<double> elapsed)
{
  std::cout << pPlayer->getName() << " (";
  double time = elapsed.count();
  if (time > 1) {
    time = int(time*1000);
    time /= 1000;
    std::cout << time << "s";
  } else if (time > .03) {
    time = int(time*1'000'0);
    time /= 10;
    std::cout << time << "ms";
  } else {
    time = int(time*1'000'000'000);
    time /= 1000;
    std::cout << time << "us";
  }
  std::cout << "): ";
}

constexpr int PLAYER_COUNT = 2;

int main()
{
  Server server;
  Battle::Bag bag;
  Battle::Dictionary dictionary("twl06.txt");
  Battle::Board board(server, bag, dictionary);
  std::array<std::unique_ptr<Battle::PlayerIf>,PLAYER_COUNT> players;
  std::array<std::string,PLAYER_COUNT> racks;
  std::array<int,PLAYER_COUNT> scores;
  players[0] = Battle::MakePlayer<0>();
  players[1] = Battle::MakePlayer<1>();
  for (int player = 0 ; player < PLAYER_COUNT ; player++ ) {
    racks[player] = bag.draw("");
    scores[player] = 0;
  }
  server.setNames(players[0]->getName(), players[1]->getName());
  int noPlayCount = 0;
  for ( int player = 0 ; ; player = (player + 1) % PLAYER_COUNT ) {
    server.setRack(racks[0], racks[1]);
    server.setScores(scores[0], scores[1]);
    players[player]->setRack(racks[player]);
    if (racks[0].size() == 0 && racks[1].size() == 0 ) {
      std::cout << "Both racks empty, time to quit\n";
      break;
    }
    if (racks[player].size() == 0 ) {
      std::cout << players[player]->getName() << " has an empty rack, skipping turn\n";
      continue;
    }
    auto t1 = std::chrono::high_resolution_clock::now();
    auto pMove = players[player]->getMove();
    auto t2 = std::chrono::high_resolution_clock::now();
    print_prefix(players[player].get(), t2 - t1);
    auto pDiscard = dynamic_cast<Battle::Discard *>(pMove.get());
    if (pDiscard) {
      std::cout << "Discarding: " << pDiscard->m_tiles << "\n";
      server.addHistory(players[player]->getName(), "Discard " + pDiscard->m_tiles, 0);
      racks[player] = bag.swap(racks[player], pDiscard->m_tiles);
      if (++noPlayCount >= 3) {
        std::cout << "After three consecutive no plays, we exit\n";
        break;
      }
    } else {
      auto pPlay = dynamic_cast<Battle::Play *>(pMove.get());
      std::cout << *pPlay;
      if (!bag.checkPlay(racks[player], pPlay->m_tiles)) {
        throw std::logic_error("Attempt to play tiles not present in rack");
      }
      auto word = board.testPlacement(pPlay->m_across, pPlay->m_row, pPlay->m_col, pPlay->m_tiles);
      if (word.m_word.size() == 0) {
        throw std::logic_error("Placement error");
      }
      auto playedWords = board.getWords(word);
      int bad_word_index = board.allValid(playedWords);
      if (bad_word_index >= 0) {
        std::cout << playedWords[bad_word_index].m_word << " is not a valid word\n";
        throw std::logic_error("Invalid play");
      }
      auto score = board.getScore(playedWords);
      if (pPlay->m_tiles.size() == 7) {
        score += 50;
      }
      scores[player] += score;
      board.play(pPlay->m_across, pPlay->m_row, pPlay->m_col, pPlay->m_tiles);
      std::cout << " " << score << "\n";

      std::stringstream ss;
      ss << char(pPlay->m_row+'A') << (pPlay->m_col+1) << "-" << (pPlay->m_across ? "A" : "D") << " " << pPlay->m_tiles;
      int row = word.m_row;
      int col = word.m_col;
      for (char c : word.m_word) {
        players[0]->layTile(row, col, c);
        players[1]->layTile(row, col, c);
        row += pPlay->m_across ? 0 : 1;
        col += pPlay->m_across ? 1 : 0;
      }
      server.addHistory(players[player]->getName(), ss.str(), score);
      racks[player] = bag.discard(racks[player], pPlay->m_tiles);
      noPlayCount = 0;
    }    
  }
  int totalPenalty = 0;
  for ( int i = 0 ; i < PLAYER_COUNT ; i++) {
    int penalty = 0;
    for (char c : racks[i]) {
      penalty += bag.m_scores[c];
    }
    if (penalty > 0 ) {
      std::cout << "Penalizing player " << i << " rack total of " << penalty << " points\n";
      scores[i] -= penalty;
      totalPenalty += penalty;
    }
  }
  for ( int i = 0 ; i < PLAYER_COUNT; i++) {
    if (totalPenalty && racks[i].size() == 0 ) {
      std::cout << "Rewarding player " << i << " penalty total of " << totalPenalty << " points\n";
      scores[i] += totalPenalty;
    }
  }
  server.setScores(scores[0], scores[1]);
  std::this_thread::sleep_for(std::chrono::seconds(1));
  return 0;
}