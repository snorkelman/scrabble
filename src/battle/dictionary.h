#include <exception>
#include <fstream>
#include <string>
#include <unordered_set>

namespace Battle {

class Dictionary
{
 public:
  Dictionary(const std::string& wordFile)
  {
    std::ifstream f(wordFile);
    if (!f) {
      throw std::logic_error("Could not open " + wordFile);
    }
    std::string word;
    while (f >> word) {
      m_words.insert(word);
    }
  }
  inline bool valid(const std::string &s) {
    return m_words.find(s) != m_words.end();
  }
 private:
  std::unordered_set<std::string> m_words;
};

}
