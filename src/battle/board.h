#ifndef _BOARD_DOT_H
#define _BOARD_DOT_H

#include <array>
#include <iostream>
#include <map>
#include <string>
#include <vector>

#include "iserver.h"

namespace Battle {

class Bag;
class Dictionary;

class Board {
 public:
  struct PlayedWord {
    bool m_across;
    int m_row;
    int m_col;
    int m_score;
    std::string m_word;
  };

  enum class Modifier {
    None,
    DoubleLetter,
    TripleLetter,
    DoubleWord,
    TripleWord
  };

  enum class OccupancyState {
    NoNeighbors,
    HasNeighbors,
    Occupied
  };

  struct Possible {
    bool m_across;
    int m_row;
    int m_col;
    size_t m_tileCount;
    std::string m_regex;
  };

  Board(IServer &server, Bag &bag, Dictionary &dictionary);
  void play( bool across, int row, int col, std::string letters);
  void occupy( int row, int col);
  //
  // This function verifies that a string can be placed at the given row and column.
  // It does two things. First, it makes sure there is room to drop the letters at the
  // given spot, and second, that at least one of the spots has a neighbor.
  // It returns the resulting word - when you play a set of tiles, the resulting
  // might have some tiles inserted in the middle due to crossing, and additional
  // prefix and postfix characters depending on where the tiles were played.
  //
  // If the play is not legal, the returned word is empty.
  //
  PlayedWord testPlacement( bool across, int row, int col, std::string letters);
  PlayedWord downSpan(int row, int col, char letter);
  PlayedWord acrossSpan(int row, int col, char letter);
  int getScore(PlayedWord &word);
  int getScore(std::vector<PlayedWord> &words);
  int allValid(const std::vector<PlayedWord> &words);
  std::vector<PlayedWord> getWords(PlayedWord &playedWord);
  std::vector<Board::Possible> legalAcross();
  void legalDown(std::vector<Possible> &possibles);
 private:
  std::array<std::array<char,15>,15> m_letters{0};
  std::array<std::array<OccupancyState,15>,15> m_occupancy;
  static std::array<std::array<Modifier,15>,15> m_modifiers;
  IServer &m_server;
  Bag &m_bag;
  Dictionary &m_dictionary;
};

std::ostream &operator<<(std::ostream &s, const Board::PlayedWord &rhs);


}

#endif // _BOARD_DOT_H