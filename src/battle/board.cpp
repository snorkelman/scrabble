#include "board.h"

#include <iostream>
#include <string>
#include <vector>

#include "bag.h"
#include "dictionary.h"
#include "server.h"

namespace Battle {

  std::array<std::array<Board::Modifier,15>,15> Board::m_modifiers = []()
  {
    std::array<std::array<Board::Modifier,15>,15> retval;
    const char *mods = "3..d...3...d..3"
                      ".2...t...t...2."
                      "..2...d.d...2.."
                      "d..2...d...2..d"
                      "....2.....2...."
                      ".t...t...t...t."
                      "..d...d.d...d.."
                      "3..d...2...d..3"
                      "..d...d.d...d.."
                      ".t...t...t...t."
                      "....2.....2...."
                      "d..2...d...2..d"
                      "..2...d.d...2.."
                      ".2...t...t...2."
                      "3..d...3...d..3";
    const char *p = mods;
    for ( int row = 0 ; row < 15 ; row++ ) 
      for ( int col = 0 ; col < 15 ; col++ ) {
        switch (*p++ ) {
          case '3' : retval[row][col] = Board::Modifier::TripleWord; break;
          case '2' : retval[row][col] = Board::Modifier::DoubleWord; break;
          case 't' : retval[row][col] = Board::Modifier::TripleLetter; break;
          case 'd' : retval[row][col] = Board::Modifier::DoubleLetter; break;
          case '.' : retval[row][col] = Board::Modifier::None; break;
          default : throw std::logic_error("board initialization not working");
        }
      }
    return retval;
  }();


  std::ostream &operator<<(std::ostream &s, const Board::PlayedWord &rhs)
  {
    s << (rhs.m_across ? "across " : "down ")
      << char('A' + rhs.m_row)
      << (rhs.m_col+1)
      << " "
      << rhs.m_word;
    if (rhs.m_score != -1) {
      s << " "
        << rhs.m_score;
    }
    return s;
  }

  std::ostream &operator<<(std::ostream &s, const Board::Possible &rhs)
  {
    s << (rhs.m_across ? "A" : " D")
      << rhs.m_tileCount
      << " "
      << char('A' + rhs.m_row)
      << (rhs.m_col+1)
      << " "
      << rhs.m_regex;
    return s;
  }

  Board::Board(IServer &server, Bag &bag, Dictionary &dictionary) 
  : m_server(server)
  , m_bag(bag)
  , m_dictionary(dictionary)
  {
    for ( int row = 0 ; row < 15 ; row++ ) {
      for ( int col = 0 ; col < 15 ; col++ ) {
        m_occupancy[row][col] = Board::OccupancyState::NoNeighbors;
      }
    }
    m_occupancy[7][7] = Board::OccupancyState::HasNeighbors;
  }

  Board::PlayedWord Board::testPlacement( bool across, int row, int col, std::string letters)
  {
    int test_row = row;
    int test_col = col;
    int col_delta = across ? 1 : 0;
    int row_delta = across ? 0 : 1;
    auto temp = letters;
    bool hasNeighbors = false;
    std::string retval;
    if (row>= 0 && row <= 14 && col >= 0 && col <= 14 && m_occupancy[row][col] == OccupancyState::Occupied)
    {
      return {across, 0, 3, -1, ""}; // can't start out on an occupied square
    }
    while (temp.size()) {
      if (test_row > 14 || test_col > 14 ) {
        return {across, 0, 1, -1, ""}; //ran out of room
      }
      if (m_occupancy[test_row][test_col] == OccupancyState::Occupied)
      {
        retval += m_letters[test_row][test_col];
      } 
      else 
      {
        retval += temp[0];
        temp.erase(0,1);
      }
      if (m_occupancy[test_row][test_col] == OccupancyState::HasNeighbors) 
      {
        hasNeighbors = true;
      }
      test_row += row_delta;
      test_col += col_delta;
    }
    if (!hasNeighbors) {
      return {across, 0, 2, -1, ""}; // no square I tested has a neighbor
    }
    //
    // now extend the word by as many letters as possible
    // 
    while (test_row < 15 && test_col < 15 && m_occupancy[test_row][test_col] == OccupancyState::Occupied) 
    {
      retval += m_letters[test_row][test_col];
      test_row += row_delta;
      test_col += col_delta; 
    }
    //
    // Prefix the word by as many letters as possible
    // 
    while ((row-row_delta) >= 0 && (col-col_delta) >= 0 && m_occupancy[row-row_delta][col-col_delta] == OccupancyState::Occupied) {
      row -= row_delta;
      col -= col_delta;
      retval = m_letters[row][col] + retval;
    }
    return {across, row, col, -1, retval};
  }

  Board::PlayedWord Board::downSpan(int row, int col, char letter) {
    std::string word(1, letter);
    int start_row = row - 1;
    while (start_row >= 0 && m_occupancy[start_row][col] == OccupancyState::Occupied) {
      word = m_letters[start_row][col] + word;
      start_row--;
    }
    int end_row = row + 1;
    while (end_row <15 && m_occupancy[end_row][col] == OccupancyState::Occupied) {
      word = word + m_letters[end_row][col];
      end_row++;
    }
    if ( word.size() == 1)  
      return {false, row, col, -1, ""};
    else
      return {false, start_row+1, col, -1, word};
  }

  Board::PlayedWord Board::acrossSpan(int row, int col, char letter) {
    std::string word(1, letter);
    int start_col = col - 1;
    while (start_col >= 0 && m_occupancy[row][start_col] == OccupancyState::Occupied) {
      word = m_letters[row][start_col] + word;
      start_col--;
    }
    int end_col = col + 1;
    while (end_col < 15 && m_occupancy[row][end_col] == OccupancyState::Occupied) {
      word = word + m_letters[row][end_col];
      end_col++;
    }
    if ( word.size() == 1)  
      return {true, row, col, -1, ""};
    else
      return {true, row, start_col+1, -1, word};
  }

  std::vector<Board::PlayedWord> Board::getWords(Board::PlayedWord &playedWord)
  {
    std::vector<Board::PlayedWord> retval{playedWord};
    int row = playedWord.m_row;
    int col = playedWord.m_col;
    int col_delta = playedWord.m_across ? 1 : 0;
    int row_delta = playedWord.m_across ? 0 : 1;
    std::string word = playedWord.m_word;
    Board::PlayedWord result;
    while ( word.size() ) {
      if (m_occupancy[row][col] != Board::OccupancyState::Occupied) {
        if (playedWord.m_across) {
          result = downSpan(row, col, word[0]);
        } else { 
          result = acrossSpan(row, col, word[0]);
        }
        if (result.m_word.size()>1) {
          retval.push_back(result);
        }
      }
      word.erase(0,1);
      row += row_delta;
      col += col_delta;
    }
    return retval;
  } 

  std::vector<Board::Possible> Board::legalAcross() {
    std::vector<Possible> retval;
    for (int row = 0 ; row < 15 ; row++ ) {
      for (int col = 0 ; col < 15 ; col++ ) {
        if (m_occupancy[row][col] != OccupancyState::Occupied) {
          bool hasNeighbors = false;
          size_t tile_count = 0;
          size_t letter_count = 0;
          int end_col = col-1;
          std::string regex;
          while (tile_count < 7 && end_col < 15) {
            end_col++;
            letter_count++;
            if (m_occupancy[row][end_col] == Board::OccupancyState::HasNeighbors) {
              hasNeighbors = true;
            }
            if (m_occupancy[row][end_col] == Board::OccupancyState::Occupied) {
              regex += m_letters[row][end_col];
            } else {
              regex += '.';
              tile_count++;
            }
            if (hasNeighbors) {
              if (letter_count > 1) {
                if (end_col == 14 || m_occupancy[row][end_col] != Board::OccupancyState::Occupied)
                {
                  retval.push_back({true, row, col, tile_count, regex});
                }
              } else if (letter_count == 1) {
                if (end_col < 14 && m_occupancy[row][end_col+1] == Board::OccupancyState::Occupied)
                {
                  retval.push_back({true, row, col, tile_count, regex});
                }
                else if (end_col > 0 && m_occupancy[row][end_col-1] == Board::OccupancyState::Occupied)
                {
                  retval.push_back({true, row, col, tile_count, regex});
                }
              }
            }
          }
        }
      }
    }
    return retval;
  }

  void Board::legalDown(std::vector<Board::Possible> &possibles) {
    for (int row = 0 ; row < 15 ; row++ ) {
      for (int col = 0 ; col < 15 ; col++ ) {
        if (m_occupancy[row][col] != OccupancyState::Occupied) {
          bool hasNeighbors = false;
          size_t tile_count = 0;
          size_t letter_count = 0;
          int end_row = row-1;
          std::string regex;
          while (tile_count < 7 && end_row < 15) {
            end_row++;
            letter_count++;
            if (m_occupancy[end_row][col] == Board::OccupancyState::HasNeighbors) {
              hasNeighbors = true;
            }
            if (m_occupancy[end_row][col] == Board::OccupancyState::Occupied) {
              regex += m_letters[end_row][col];
            } else {
              regex += '.';
              tile_count++;
            }
            if (hasNeighbors) {
              if (letter_count > 1) {
                if (end_row == 14 || m_occupancy[end_row][col] != Board::OccupancyState::Occupied)
                {
                  possibles.push_back({false, row, col, tile_count, regex});
                }
              } else if (letter_count == 1) {
                if (end_row < 14 && m_occupancy[end_row+1][col] == Board::OccupancyState::Occupied)
                {
                  possibles.push_back({false, row, col, tile_count, regex});
                }
                else if (end_row > 0 && m_occupancy[end_row-1][col] == Board::OccupancyState::Occupied)
                {
                  possibles.push_back({false, row, col, tile_count, regex});
                }
              }
            }
          }
        }
      }
    }
  }

  void Board::occupy( int row, int col)
  {
    m_occupancy[row][col] = OccupancyState::Occupied;
    if (row > 0 && m_occupancy[row-1][col] == OccupancyState::NoNeighbors) {
      m_occupancy[row-1][col] = OccupancyState::HasNeighbors;
    }
    if (row < 14 && m_occupancy[row+1][col] == OccupancyState::NoNeighbors) {
      m_occupancy[row+1][col] = OccupancyState::HasNeighbors;
    }
    if (col > 0 && m_occupancy[row][col-1] == OccupancyState::NoNeighbors) {
      m_occupancy[row][col-1] = OccupancyState::HasNeighbors;
    }
    if (col < 14 && m_occupancy[row][col+1] == OccupancyState::NoNeighbors) {
      m_occupancy[row][col+1] = OccupancyState::HasNeighbors;
    }
  }

  void Board::play( bool across, int row, int col, std::string letters)
  {
    while (letters.size()) {
      if (row > 14 || col > 14 ) {
        throw std::logic_error("Error placing tiles");
      }
      if (m_letters[row][col] == 0) {
        m_letters[row][col] = letters.front();
        m_modifiers[row][col] = Modifier::None;
        std::string letter{letters.front()};
        letters.erase(0,1);
        m_server.layAcross( std::string(1,'a'+row)+std::to_string(col+1), letter);
        occupy(row,col);
      }
      if (across)
        col += 1;
      else
        row += 1;
    }
  }

  int Board::getScore(PlayedWord &word)
  {
    int word_multiplier = 1;
    int raw_score = 0;
    int row_delta = word.m_across ? 0 : 1;
    int col_delta = word.m_across ? 1 : 0;
    int row = word.m_row;
    int col = word.m_col;
    for ( int i = 0 ; i < word.m_word.size() ; i++ )
    {
      switch (m_modifiers[row][col]) {
        case Board::Modifier::TripleWord : word_multiplier *= 3; raw_score += m_bag.m_scores[word.m_word[i]]; break;
        case Board::Modifier::DoubleWord : word_multiplier *= 2; raw_score += m_bag.m_scores[word.m_word[i]]; break;
        case Board::Modifier::TripleLetter : raw_score += m_bag.m_scores[word.m_word[i]] * 3; break;
        case Board::Modifier::DoubleLetter : raw_score += m_bag.m_scores[word.m_word[i]] * 2; break;
        case Board::Modifier::None : raw_score += m_bag.m_scores[word.m_word[i]]; break;
        default : throw std::logic_error("Invalid data in m_modifiers"); break;
      }
      row += row_delta;
      col += col_delta;
    }
    word.m_score = raw_score * word_multiplier;
    return word.m_score;
  }

  int Board::getScore(std::vector<PlayedWord> &words)
  {
    int retval = 0;
    for (auto &word : words) {
      int score = getScore(word);
      retval += score;
    }
    return retval;
  }

  int Board::allValid(const std::vector<PlayedWord> &words)
  {
    int i = 0;
    for (auto &word : words) {
      std::string w = word.m_word;
      for (auto &c : w) {
        c = std::tolower(c);
      }
      if (!m_dictionary.valid(w) ) {
        return i;
      } else {
        i++;
      }
    }
    return -1;
  }
}