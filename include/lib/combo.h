#ifndef COMBO_DOT_H
#define COMBO_DOT_H

#include <algorithm>
#include <set>
#include <string>

inline auto combinations(std::string s)
{
  std::set<std::string> retval;
  for ( int K = 2 ; K <= s.size() ; K++ ) {
    std::sort(s.begin(), s.end());
    do {
      retval.insert(s.substr(0, K));
    } while (std::next_permutation(s.begin(), s.end()));
  }
  return retval;
}

#endif // #ifdef COMBO_DOTH
