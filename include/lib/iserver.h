#ifndef _ISERVER_DOT_H
#define _ISERVER_DOT_H

#include <string>

class IServer {
 public:
   virtual ~IServer(){}
   virtual void setRack(const std::string &player_1_letters, const std::string &player_2_letters="") = 0;
   virtual void lay(bool across, const std::string &position, const std::string &letters) = 0;
   virtual void setScores(int player1, int player2) = 0;
   virtual void setNames(const std::string &player_1_name, const std::string &player_2_name) = 0;
   virtual void addHistory(const std::string &player_name, const std::string &move, int score ) = 0;
   virtual void reset() = 0;
   void layAcross(const std::string &position, const std::string &letters)
   {
       lay(true, position, letters);
   }
   void layDown(const std::string &position, const std::string &letters)
   {
       lay(false, position, letters);
   }
};
#endif //#ifndef _ISERVER_DOT_H