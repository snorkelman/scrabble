#ifndef _SERVER_DOT_H
#define _SERVER_DOT_H

#include <atomic>
#include <chrono>
#include <condition_variable>
#include <mutex>
#include <thread>

#include "iserver.h"

class Server : public IServer {
 public:
   Server();
   ~Server();
   void setRack(const std::string &player_1_letters, const std::string &player_2_letters="") override;
   void lay(bool across, const std::string &position, const std::string &letters) override;
   void setScores(int player1, int player2) override;
   void setNames(const std::string &player_1_name, const std::string &player_2_name) override;
   void addHistory(const std::string &player_name, const std::string &move, int score ) override;
   void reset() override;
   static constexpr const char *m_host="localhost";
   static constexpr int m_port=8888;
 private:
   void stop();
   void forceServe();
   void http_server();
   std::string m_player_1_rack;
   std::string m_player_2_rack;
   std::string m_player_1_name;
   std::string m_player_2_name;
   std::string m_board;
   std::string m_history;
   std::atomic<int> m_player1{0};
   std::atomic<int> m_player2{0};
   std::thread m_thread;
   static constexpr std::chrono::seconds m_longPollTimeout{120};

   std::mutex m_rackMutex;
   std::condition_variable m_rackConditionVariable;
   std::atomic<bool> m_newRackData{false};

   std::mutex m_boardMutex;
   std::condition_variable m_boardConditionVariable;
   std::atomic<bool> m_newBoardData{false};

   std::mutex m_scoreMutex;
   std::condition_variable m_scoreConditionVariable;
   std::atomic<bool> m_newScoreData{false};

   std::mutex m_historyMutex;
   std::condition_variable m_historyConditionVariable;
   std::atomic<bool> m_newHistoryData{false};

   std::atomic<bool> m_shutDown{false};
};

#endif // #ifndef _SERVER_DOT_H
