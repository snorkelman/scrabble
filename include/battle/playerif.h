#ifndef PLAYER_IF_DOT_H
#define PLAYER_IF_DOT_H

#include <iostream>
#include <memory>
#include <string>

namespace Battle
{
  struct Move {
    virtual ~Move(){}
  };

  struct Discard : public Move {
    Discard(const std::string &tiles) : m_tiles(tiles){}
    std::string m_tiles;
  };

  struct Play : public Move {
    Play(bool across, int row, int col, const std::string &tiles)
    : m_across(across)
    , m_row(row)
    , m_col(col)
    , m_tiles(tiles)
    {}
    bool m_across;
    int m_row;
    int m_col;    
    std::string m_tiles;
  };

  class PlayerIf {
   public:
    virtual std::string getName() = 0;
    virtual void setRack(std::string letters) = 0; 
    virtual void layTile(int row, int col, char tile) = 0;
    virtual std::unique_ptr<Move> getMove() = 0;
  };

template<int N>
std::unique_ptr<Battle::PlayerIf> MakePlayer();

}

#endif // #ifndef PLAYER_IF_DOT_H